package com.example.tbcroomfirstexample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.tbcroomfirstexample.databinding.FragmentAddUserBinding
import com.example.tbcroomfirstexample.model.User

class AddUserFragment : Fragment() {
    private var _binding: FragmentAddUserBinding? = null
    private val binding: FragmentAddUserBinding get() = _binding!!
    private val mainViewModel: MainViewModel by viewModels()

    val profileImageList = mutableListOf(
        "https://upload.wikimedia.org/wikipedia/commons/4/41/Profile-720.png",
        "https://www.searchpng.com/wp-content/uploads/2019/02/Men-Profile-Image-715x657.png",
        "https://img.pngio.com/edit-profile-icon-profile-png-1600_1600.png",
        "https://cdn2.iconfinder.com/data/icons/avatars-99/62/avatar-370-456322-512.png"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddUserBinding.inflate(layoutInflater, container, false)
        init()
        return _binding?.root
    }

    private fun init() {
        binding.btnSave.setOnClickListener {
            setData()

        }
        setHeightSpinner()
        val profileImage = profileImageList.random()
        Glide.with(requireContext()).load(profileImage).into(binding.ivProfile)
    }

    private fun setData() {
        val profileImage = profileImageList.random()

        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val age = binding.etAge.text.toString()
        val address = binding.etAddress.text.toString()
        val height = binding.dropDownHeight.text.toString()
        if (firstName.isNotEmpty() && lastName.isNotEmpty()
            && age.isNotEmpty() && address.isNotEmpty()
        ) {
            val user = User(0, firstName, lastName, age.toInt(), address, height, profileImage)
            mainViewModel.insertUser(user)
        }

    }

    private fun setHeightSpinner() {
        val heights = resources.getStringArray(R.array.heights)
        val adapter = ArrayAdapter(requireContext(), R.layout.drop_down_item, heights)
        binding.dropDownHeight.setAdapter(adapter)

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}