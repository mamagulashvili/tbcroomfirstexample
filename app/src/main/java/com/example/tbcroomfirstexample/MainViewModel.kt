package com.example.tbcroomfirstexample

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcroomfirstexample.db.UserDb
import com.example.tbcroomfirstexample.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel : ViewModel() {
    init {
        getAllUser()
    }

    fun insertUser(user: User) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            UserDb.db.getUserDao().insertUser(user)
        }
    }

    private val users = MutableLiveData<List<User>>()
    val _users: LiveData<List<User>> = users

    fun getAllUser() = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val result = UserDb.db.getUserDao().getAllUser()
            users.postValue(result)
        }
    }
}