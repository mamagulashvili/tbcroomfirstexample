package com.example.tbcroomfirstexample

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcroomfirstexample.databinding.RowItemBinding
import com.example.tbcroomfirstexample.model.User

class UserListAdapter : RecyclerView.Adapter<UserListAdapter.UserViewHolder>() {

    val differCallBack = object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, differCallBack)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class UserViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val user = differ.currentList[adapterPosition]
            binding.apply {
                tvAddress.text = user.address
                tvAge.text = user.age.toString()
                tvFirstName.text = user.firstName
                tvLastName.text = user.lastName
                tvHeight.text = user.height
            }
            itemView.apply {
                Glide.with(this).load(user.profile).into(binding.ivProfile)
            }
        }
    }

}