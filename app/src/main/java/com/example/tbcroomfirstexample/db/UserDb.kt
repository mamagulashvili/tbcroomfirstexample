package com.example.tbcroomfirstexample.db

import androidx.room.Room
import com.example.tbcroomfirstexample.App

object UserDb {
    val db = Room.databaseBuilder(
        App.context!!,
        UserDatabase::class.java, "users_database"
    ).build()
}