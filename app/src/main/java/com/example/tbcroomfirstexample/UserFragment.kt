package com.example.tbcroomfirstexample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.tbcroomfirstexample.databinding.FragmentUserBinding


class UserFragment : Fragment() {
    private var _binding: FragmentUserBinding? = null
    private val binding: FragmentUserBinding get() = _binding!!
    private val listAdapter: UserListAdapter by lazy { UserListAdapter() }
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUserBinding.inflate(layoutInflater, container, false)
        init()
        return _binding?.root
    }

    private fun init() {
        observe()
        binding.fabAddUser.setOnClickListener {
            findNavController().navigate(R.id.action_userFragment_to_addUserFragment)
        }
    }

    private fun observe() {
        mainViewModel._users.observe(viewLifecycleOwner, {
            listAdapter.differ.submitList(it)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}